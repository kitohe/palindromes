# Palindromes

1. Reading input from program arguments line, as palindromes (multiple) to check
2. In case of not passing any arguments standard menu shall be open
3. If given word as an input **is** palindrome, the appropriate statement will be printed
4. If given word as an input **isn't** palidrome, as return to view list of differences will be passed
5. Valid format for checking differences in words that **are not** palindromes is as follows:
    - Array shall be filled in function that checks palindromes, 
    - Empty list or *isPalindrome* variable set means that word was indeed palindrome 
    - List that is **not** empty have differences strored like this: first index is letter of original string, second index is letter of reversed string. Letters **will** be printed to show differences, List  **will not** contain letters that were occurring at the same index.
6. ...

Excercise 2 requirements:

- (DONE) pe�ny zakres raportu nr 1,
- (DONE) dodanie typowo-bezpiecznej kolekcji obiekt�w do klasy modelu,
- (DONE) wykorzystanie standardowych adnotacji,
- (DONE) wykorzystanie p�tli for-each
- (DONE) wykorzystanie jednego z nast�puj�cych element�w:
	- (DONE) typ wyliczeniowy,
	- w�asny typ generyczny,
	- zmienna liczba parametr�w metody,
	- w�asna adnotacja (inna ni� w przyk�adach demonstracyjnych),
	- wyra�enie lambda (ze zdefiniowanym interfejsem).
- (DONE) zdefiniowanie test�w jednostkowych dla wszystkich publicznych metod znajduj�cych si� w modelu (za wyj�tkiem konstruktor�w, akcesor�w i mutator�w) 
- (DONE) testy powinny obejmowa� sytuacje poprawne, niepoprawne i graniczne dla ka�dej testowanej jednostki,
