package com.madrala.palindromes.exception;

/**
 * @author  Daniel Madrala
 * @version 0.2.1
 * @since 0.1
 */
public class InvalidWordException extends Exception {

    /**
     * Constructor for exception object
     * @param what happened
     */
    public InvalidWordException(String what) {
        super(what);
    }
}
