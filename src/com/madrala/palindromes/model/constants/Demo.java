package com.madrala.palindromes.model.constants;

/**
 * @author  Daniel Madrala
 * @version 0.2.1
 * @since 0.2
 */
public class Demo {

    /**
     * Defines enum type of words that are palindromes
     * to run them in demo mode
     */
    public enum Palindrome {
        ANNA, KAYAK, LEVEL, MADAM, NOON, RADAR
    }

    /**
     * Defines enum type of words that are NOT palindromes
     * to run them in demo mode
     */
    public enum NotPalindrome {
        CAT, DOG, KEYBOARD, MONITOR, BOOK, COOK
    }
}
