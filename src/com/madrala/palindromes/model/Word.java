package com.madrala.palindromes.model;

import com.madrala.palindromes.exception.InvalidWordException;

import java.util.ArrayList;

/**
 * @author  Daniel Madrala
 * @version 0.2.1
 * @since 0.1
 */
public class Word {

    /**
     * Contains string that user has entered
     */
    private String word;

    /**
     * Contains string that has been reversed
     */
    private String reversed;

    /**
     * Contains list of differences between string and reversed string
     */
    private ArrayList<Character> differences;

    /**
     * To determine if word was classified as palindrome
     */
    private boolean isPalindrome;

    /**
     * Constructor for Word object, initializes all class fields with appropriate values
     * @param inputString that user has passed
     */
    public Word(String inputString) {
        word = inputString;
        reversed = new StringBuilder(word).reverse().toString();
        isPalindrome = false;
        differences = new ArrayList<>();
    }

    /**
     * Getter for word field
     * @return word field
     */
    public String getWord() { return this.word; }

    /**
     * Getter for reversed string field
     * @return reversed string field
     */
    public String getReversed() { return this.reversed; }

    /**
     * Getter for isPalindrome field
     * @return isPalindrome field
     */
    public boolean getIsPalindrome() { return this.isPalindrome; }

    /**
     * Getter for ArrayList differences field
     * @return ArrayList differences field
     */
    public ArrayList<Character> getDifferences() { return this.differences; }

    /**
     * Checks weather word is valid palindrome
     * @throws InvalidWordException when number in given string has occurred
     * or given string was empty (considered empty when containing white spaces)
     */
    public void checkIfPalindrome() throws InvalidWordException {

        word = word.replaceAll("\\s", "");

        if(word.length() == 0) throw new InvalidWordException("Given word was empty\n");

        boolean hasDigit = word.matches(".*\\d+.*");

        if(hasDigit) throw new InvalidWordException("Given word: " + this.word);

        if(word.equals(reversed)){
            isPalindrome = true;
        } else {
            calculateDifferences();
        }
    }

    /**
     * Calculates differences between original string entered by user and reversed version of it
     * Results are being stored in differences ArrayList
     */
    private void calculateDifferences() {
        int size = word.length();

        for(int i = 0; i < size ; i++) {
            if(word.charAt(i) != reversed.charAt(i)) {
                differences.add(word.charAt(i));
                differences.add(reversed.charAt(i));
            }
        }
    }
}
