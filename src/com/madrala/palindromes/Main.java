package com.madrala.palindromes;

import com.madrala.palindromes.controller.MenuController;
import com.madrala.palindromes.controller.WordController;

/**
 * @author  Daniel Madrala
 * @version 0.2.1
 * @since 0.1
 */
public class Main {

    /**
     * Starting method
     * @param args Parameters that are being passed to program when its being started;
     *             that are words that will be check weather they are palindromes or not
     */
    public static void main(String[] args) {
        MenuController menuController = new MenuController();

        if (args.length > 0) {
            WordController wordController = new WordController();

            for (String wordToCheck :
                    args) {
                wordController.checkFromArguments(wordToCheck);
            }

            System.exit(0);
        }
        menuController.runMenu();
    }
}
