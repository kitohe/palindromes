package com.madrala.palindromes.view;

import com.madrala.palindromes.model.Word;

import java.util.Scanner;

/**
 * @author  Daniel Madrala
 * @version 0.2.1
 * @since 0.1
 */
public class WordView {
    /**
     * Scanner field to read user's inputs
     */
    private final Scanner scanner;

    /**
     * Default constructor for WordView class
     */
    public WordView() {
        this.scanner = new Scanner(System.in);
    }

    /**
     * Collects word to process from user
     * @return word typed by user
     */
    public String checkPalindromeMenu() {
        String inputString;

        System.out.println("Input string: ");

        return inputString = scanner.next();
    }

    /**.
     * Prints information about palindrome
     * @param palindrome word that is palindrome
     */
    public void palindrome(Word palindrome) {
        System.out.println("Given word: " + palindrome.getWord() +
                ", Reversed: " + palindrome.getReversed() +
                " | This word is PALINDROME\n");
    }

    /**
     * Prints information about word that is not palindrome
     * @param notPalidrome wort that is not palindrome
     */
    public void notPalindrome(Word notPalidrome) {
        System.out.println("Given word: " + notPalidrome.getWord() +
                ", Reversed: " + notPalidrome.getReversed() +
                " is NOT PALINDROME\n" +
                "Differences:");
        for(int i = 0; i < notPalidrome.getDifferences().size(); i+=2) {
            System.out.println("Should be: " + notPalidrome.getDifferences().get(i) + " is: "
                    + notPalidrome.getDifferences().get(i+1));
        }
    }

    public void demoPalindromes() {
        System.out.println("---------------------------------------------------------------------\n" +
                "This is demo run to show how program works:\n" +
                "---------------------------------------------------------------------");
    }

    public void demoNotPalindromes() {
        System.out.println("---------------------------------------------------------------------\n" +
                "This part of demo is showing output that program is going to print when word that" +
                "is not a palindrome will be entered\n" +
                "---------------------------------------------------------------------");
    }

    /**
     * Executed when user's input will be incorrect
     */
    public void userInputError() { System.err.println("Cannot parse that word because it contain numbers or word is empty!"); }

    /**
     * Prints debug info passed to it
     * @deprecated as of version 0.2 replaced by printDebugInfo
     * @param info to print out in console
     */
    @Deprecated
    public void debugInfo(String info) {
        System.out.println("\nDEBUGGING\n Info: " + info);
    }

    /**
     * Prints debugging information passed into this method
     * @param info to print out in console
     */
    public void printDebugInfo(String info) {
        System.out.println("Log: " + info + "\n");
    }
}
