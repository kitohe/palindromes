package com.madrala.palindromes.view;

import java.util.Scanner;

/**
 * @author  Daniel Madrala
 * @version 0.1
 * @since 0.1
 */
public class MenuView {
    /**
     * Scanner field to read user's inputs
     */
    private final Scanner scanner;

    /**
     * Default constructor for MenuView Class
     */
    public MenuView() { this.scanner = new Scanner(System.in); }

    /**
     * Displays welcome menu on user's screen
     * @return choice of user
     */
    public int welcome() {
            int choice;

            System.out.println("Palindromes program by DM\n" +
                    "1. Check if palindrome\n" +
                    "2. Run demo\n" +
                    "3. Exit\n" +
                    "Choice: ");

            choice = nextValidInt(scanner);
            return choice;
        }

    /**
     * Collects next valid integer from user
     * @param scanner that is being currently used
     * @return integer number selected by user
     */
    private int nextValidInt(Scanner scanner) {
           while(!scanner.hasNextInt()) {
               System.out.println(scanner.next() + " is not valid option. Try again: ");
           }
           return scanner.nextInt();
       }
}
