package com.madrala.palindromes.controller;

import com.madrala.palindromes.view.MenuView;

/**
 * @author  Daniel Madrala
 * @version 0.2.1
 * @since 0.1
 */
public class MenuController {
    /**
     * MenuView object for displaying on user's screen
     */
    private final MenuView menuView;

    /**
     * WordController object for passing workload to that Class
     */
    private final WordController wordController;

    /**
     * Default constructor for MenuController Class
     */
    public MenuController() {
        this.menuView = new MenuView();
        this.wordController = new WordController();
    }

    /**
     * Initializes view, and user interaction
     */
    public void runMenu() {
        int choice = menuView.welcome();

        switch(choice) {
            case 1:
                wordController.setWord();
                break;
            case 2:
                wordController.runDemo();
                break;
            case 3:
                System.exit(0);
        }
    }
}
