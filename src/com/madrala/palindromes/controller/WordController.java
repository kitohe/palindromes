package com.madrala.palindromes.controller;

import com.madrala.palindromes.exception.InvalidWordException;
import com.madrala.palindromes.model.constants.Demo;
import com.madrala.palindromes.model.Word;
import com.madrala.palindromes.view.WordView;

/**
 * @author  Daniel Madrala
 * @version 0.2.1
 * @since 0.1
 */
public class WordController {
    /**
     * WordView object for displaying information about entered
     * word by user
     */
    private final WordView wordView;

    /**
     * Default constructor for WordController Class
     * Initializes menuView and palindromeController variables
     */
    public WordController() {
        this.wordView = new WordView();
    }


    /**
     * Launches chain to check weather input string is
     * valid palindrome
     * @param inputString to be checked
     */
    public void checkFromArguments(String inputString) {
        checkResults(initialize(inputString));
    }

    /**
     * To check results from given input
     * @param word that has been validated and according
     *                   wordView option is going to be displayed
     */
    private void checkResults(Word word) {
        if(word == null) {
            wordView.userInputError();
        } else if(word.getIsPalindrome()) {
            wordView.palindrome(word);
        } else {
            wordView.notPalindrome(word);
        }
    }

    /**
     * Runs program to determine weather word is palindrome
     */
    public void setWord() {
        checkResults(initialize(wordView.checkPalindromeMenu()));
    }

    /**
     * Runs program in DEMO mode to show how program works with words that are
     * palindrome or not
     */
    public void runDemo() {
        wordView.demoPalindromes();
        for (Demo.Palindrome pal :
                Demo.Palindrome.values()) {
            checkResults(initialize(pal.toString()));
        }
        wordView.demoNotPalindromes();
        for (Demo.NotPalindrome notPal :
                Demo.NotPalindrome.values()) {
            checkResults(initialize(notPal.toString()));
        }
    }

    /**
     * Initializes new Word object to validate weather it is valid palindrome or not
     * @param inputString that is going to be validated
     * @return Word object that has been validated
     */
    private Word initialize(String inputString) {
        Word word = new Word(inputString);

        try {
            word.checkIfPalindrome();
        } catch (InvalidWordException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return word;
    }
}
