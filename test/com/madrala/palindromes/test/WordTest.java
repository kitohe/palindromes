package com.madrala.palindromes.test;

import com.madrala.palindromes.exception.InvalidWordException;
import com.madrala.palindromes.model.Word;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.fail;

/**
 * @author  Daniel Madrala
 * @version 0.2.1
 * @since 0.2
 */
public class WordTest {

    /**
     * Test valid palindrome
     * @result will be successful when program recognizes that word was indeed palindrome
     * thus isPalindrome flag will be set to true and reversed word should equal not reversed
     */
    @Test
    public void checkIfPalindrome() {
        Word possiblePalindrome = new Word("abba");
        String expectedOutput = "abba";

        try {
            possiblePalindrome.checkIfPalindrome();
            Assert.assertEquals(possiblePalindrome.getReversed(), expectedOutput);
            Assert.assertTrue(possiblePalindrome.getIsPalindrome());
        } catch (InvalidWordException e) {
            System.err.println(e.getMessage());
        }
    }


    /**
     * Test word that contains number thus is illegal
     * @result Should catch InvalidWordException
     */
    @Test
    public void checkIfWordContainNumbers() {
        Word possiblePalindrome = new Word("race4car");

        try {
            possiblePalindrome.checkIfPalindrome();
            fail("InvalidWordException was not thrown for some reason");
        } catch (InvalidWordException e){
            // GOOD; NO NUMBERS ALLOWED
        }
    }

    /**
     * Test not valid palindrome
     * @result will be successful when isPalindrome flag will be set by program for this word to false
     * and correct ArrayList of differences will be populated
     */
    @Test
    public void wordIsNotPalindrome() {
        Word definitelyNotPalindrome = new Word("not");
        ArrayList<Character> diff = new ArrayList<>();

        diff.add('n');
        diff.add('t');
        diff.add('t');
        diff.add('n');

        try {
            definitelyNotPalindrome.checkIfPalindrome();
            Assert.assertEquals(diff, definitelyNotPalindrome.getDifferences());
            Assert.assertFalse(definitelyNotPalindrome.getIsPalindrome());
        } catch (InvalidWordException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Test with invalid user input
     * @result Should catch InvalidWordException
     */
    @Test
    public void emptyString() {
        // WHITE SPACES
        Word emptyWord = new Word("                 ");

        try {
            emptyWord.checkIfPalindrome();
            fail("InvalidWordException was not thrown for some reason");
        } catch (InvalidWordException e) {
            //GOOD; NO EMPTY STRINGS ALLOWED
        }
    }
}
